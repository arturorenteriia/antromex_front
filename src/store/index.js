import Vue from 'vue'
import Vuex from 'vuex'

// import example from './module-example'
import general from './general'

Vue.use(Vuex)

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Store instance.
 */

export default function( /* { ssrContext } */ ) {
    const Store = new Vuex.Store({
        modules: {
            general
        },
        mutations: {
            login(state) {
                state.general.login.isLogged = true;
            },
            logout(state) {
                state.general.login.isLogged = false;
            },
            saveUserData(state, payload) {
                state.general.user.name = payload.name;
                state.general.user.lastName = payload.last_name;
                state.general.user.id = payload.id;
                state.general.user.role = payload.roles[0];
                state.general.role.name = payload.roles[0].name;
                state.general.user.permissions = payload.roles[0].permissions;
            },

            loading(state, payload) {
                state.general.loading = payload;
            },
        },

        // enable strict mode (adds overhead!)
        // for dev mode only
        strict: process.env.DEV
    })

    return Store
}