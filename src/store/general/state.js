export default {
    login: {
        issLogged: false,
    },
    loading: false,
    apiUrl: 'https://antromexpvr.com/api/antromex/api',
    //apiUrl: 'http://antromex.test/api',
    apiAssets: 'https://antromexpvr.com/api/antromex/app',
    //apiAssets: 'http://antromex.test/app',
    user: {
        name: '',
        lastName: '',
        id: '',
        role: '',
        permissions: ''
    },
    role:{
        names:'s',
    },

}