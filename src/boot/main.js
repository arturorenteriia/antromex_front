// import something here
import axios from 'axios'
import { Cookies } from 'quasar'
// "async" is optional
export default async({ Vue, app, store, router /* app, router, Vue, ... */ }) => { // something to do
    var value = Cookies.get('token');
    var date = Cookies.get('expiredDate');
    store.commit("loading", true);
    if ((value !== undefined && value !== null) && new Date(date) > new Date()) {
        axios.interceptors.request.use(
            config => {
                config.headers.Accept = "application/json";
                config.headers.Authorization =
                    "Bearer " + value;
                return config;
            },
            function(error) {
                return Promise.reject(error);
            }
        );
        axios
            .get(store.state.general.apiUrl + "/auth/getDataUser", {}, {})
            .then(responseUser => {
                store.commit("loading", false);
                store.commit("saveUserData", responseUser.data);
               //router.push({ path: "/dashboard" });

            })
            .catch(error => {
                router.push({ path: "/login" });
                store.commit("loading", false);
            });
        store.commit("login");
    } else {
        Cookies.remove('token');
        Cookies.remove('expiredDate');

        router.push({ path: "/login" });
        store.commit("loading", false);

    }
}