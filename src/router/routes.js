import store from '../store/general/state.js';
const ifAuthenticated = (to, from, next, permisos) => {

    if (store.login.isLogged) {
        console.log(to.path);
        if(to.path == "/dashboard" || to.path == "/") {
            return getIndexView(to, from, next, permisos)
        };
        console.log(to.path);
        next()
        return
    }
    next('/login')
}

const getIndexView = (to, from, next, permisos) => {
    console.log("entro al get index");
    if(store.role.name == "vaca"){
        next("/reportes/tickets-generados-vaca");
        return
    }else{
        console.log("no es vaca");
        next()
        return
    }
}

const getReportsTicketsView = (to, from, next, permisos) => {
    if(store.role.name == "vaca"){
        next("/reportes/tickets-generados-vaca");
        return
    }else{
        next()
        return
    }
}

const routes = [
    {
        path: '/',
        component: () =>
            import ('layouts/MainLayout.vue'),
            beforeEnter: getIndexView,
        children: [{
            path: '',
            component: () =>
                import ('pages/Index.vue'),
            beforeEnter: getIndexView
        }],
        beforeEnter: ifAuthenticated,
    },
    {
        path: '/dashboard',
        component: () =>
            import ('layouts/MainLayout.vue'),
        children: [{
            path: '/dashboard',
            name:'index',
            component: () =>
            import ('pages/Index.vue'),
            beforeEnter: getIndexView,
        }],
        beforeEnter: ifAuthenticated,
    },
    {
        path: '/login',
        component: require('pages/login/login.vue').default,
    },
    {
        path: '/usuarios',
        component: () =>
            import ('layouts/MainLayout.vue'),
        children: [{
                path: '/',
                component: () =>
                    import ('pages/users/UsersTableComponent.vue')
            },
            {
                path: 'crear-usuario',
                component: () =>
                    import ('pages/users/UserCreateForm.vue')
            },
            {
                path: 'editar-usuario/:id',
                component: () =>
                    import ('pages/users/UserCreateForm.vue')
            },
            {
                path: 'roles',
                component: () => import('pages/users/roles/rolesTableComponent.vue')
            }
        ],
        beforeEnter: ifAuthenticated,
    },
    {
        path: '/establecimientos',
        component: () =>
            import ('layouts/MainLayout.vue'),
        children: [{
                path: '/',
                component: () =>
                    import ('pages/products/Products.vue')
            },
            {
                path: 'create',
                component: () =>
                    import ('pages/products/formProducts.vue')
            },
            {
                path: 'edit/:id',
                name: 'editProduct',
                component: () =>
                    import ('pages/products/formProductsEdit.vue')
            }
        ],
        beforeEnter: ifAuthenticated,

    },
    {
        path: '/socios',
        component: () =>
            import ('layouts/MainLayout.vue'),
        children: [{
                path: '/',
                component: () =>
                    import ('pages/partners/Partners.vue')
            },
            {
                path: 'create',
                component: () =>
                    import ('pages/partners/FormPartners.vue')
            },
            {
                path: 'edit/:id',
                name: 'editParters',
                component: () =>
                    import ('pages/partners/FormPartnersEdit.vue')
            }
        ],
        beforeEnter: ifAuthenticated,
    },
    {
        path: '/tickets',
        component: () =>
            import ('layouts/MainLayout.vue'),
        children: [{
                path: '/',
                name: 'tickets',
                component: () =>
                    import ('pages/tickets/tickets.vue')
            },
            {
                path: 'create',
                component: () =>
                    import ('pages/tickets/formTicket.vue')
            },
            {
                path: 'edit/:id',
                name: 'editTicket',
                component: () =>
                    import ('pages/tickets/formTicketEdit.vue')
            },
            {
                path: 'por-socio/:id',
                component: () =>
                    import ('pages/tickets/generatedTicketsByPartner.vue')
            },
        ],
        beforeEnter: ifAuthenticated,

    },
    {
        path: '/reportes',
        component: () =>
            import ('layouts/MainLayout.vue'),
        children: [{
                path: '/',
                name: 'tickets',
                component: () =>
                    import ('pages/reports/reports.vue')
            },
            {
                path: 'tickets-generados',
                name: 'generatedTickets',
                component: () =>
                    import ('pages/reports/generatedTickets.vue'),
                beforeEnter: getReportsTicketsView,
            },
            {
                path: 'tickets-generados-vaca',
                name: 'generatedTickets',
                component: () =>
                    import ('pages/reports/generatedTicketsVaca.vue'),
            },

            {
                path: 'pagos-realizados',
                name: 'generatedPayments',
                component: () =>
                    import ('pages/reports/generatedPayments.vue')
            },
            {
                path: 'saldos-vs-ingresos',
                name: 'saldosvsingresos',
                component: () =>
                    import ('pages/reports/saldosVSingresos.vue')
            },
            {
                path: 'comisiones',
                name: 'comissions',
                component: () =>
                    import ('pages/reports/comissions.vue')
            }
        ],
        beforeEnter: ifAuthenticated,

    },
    {
        path: '/saldos',
        component: () =>
            import ('layouts/MainLayout.vue'),
        children: [{
                path: '/',
                name: 'Accounts',
                component: () =>
                    import ('pages/accounts/accounts.vue')
            },


        ],
        beforeEnter: ifAuthenticated,

    },
    {
        path: '/saldos/vaca-argentina',
        component: () =>
            import ('layouts/MainLayout.vue'),
        children: [{
                path: '/',
                name: 'Accounts',
                component: () =>
                    import ('pages/accounts/accounts-vaca-argentina.vue')
            },


        ],
        beforeEnter: ifAuthenticated,

    },
    {
        path: '/pagos',
        component: () =>
            import ('layouts/MainLayout.vue'),
        children: [{
                path: '/',
                component: () =>
                    import ('pages/pagos/PaymentsComponent.vue')
            },
            {
                path: 'generar/:ticketID?',
                component: () =>
                    import ('pages/pagos/PaymentForm.vue')
            },
            {
                path: 'editar/:paymentID?',
                component: () =>
                    import ('pages/pagos/PaymentForm.vue')
            }
        ],
        beforeEnter: ifAuthenticated,

    },

    {
        path: '/configuracion',
        component: () =>
            import ('layouts/MainLayout.vue'),
        children: [{
            path: '',
            component: () =>
                import ('pages/settings/MenuSettings.vue')
        }],
        beforeEnter: ifAuthenticated,

    },
    {
        path: '/servicios',
        component: () =>
            import ('layouts/MainLayout.vue'),
        children: [{
            path: '',
            component: () =>
                import ('pages/settings/services/servicios.vue')
        }],
        beforeEnter: ifAuthenticated,
    },
    {
        path: '/concierges',
        component: () =>
            import ('layouts/MainLayout.vue'),
        children: [{
                path: '/',
                component: () =>
                    import ('pages/concierges/Concierges.vue')
            },
            {
                path: 'create',
                component: () =>
                    import ('pages/concierges/FormConcierge.vue')
            },
            {
                path: 'edit/:id',
                name: 'editParters',
                component: () =>
                    import ('pages/concierges/FormConciergeEdit.vue')
            }
        ],
        beforeEnter: ifAuthenticated,
    },
    {
        path: '/rps',
        component: () =>
            import ('layouts/MainLayout.vue'),
        children: [{
                path: '/',
                component: () =>
                    import ('pages/rps/Rp.vue')
            },
            {
                path: 'create',
                component: () =>
                    import ('pages/rps/FormRp.vue')
            },
            {
                path: 'edit/:id',
                name: 'editRP',
                component: () =>
                    import ('pages/rps/FormRp.vue')
            }
        ],
        beforeEnter: ifAuthenticated,
    },
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
    routes.push({
        path: '*',
        component: () =>
            import ('pages/Error404.vue')
    })
}

export default routes