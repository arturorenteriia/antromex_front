export default {
    data() {
        return {
            activeModel: null,
            fileID: '', // Variable para mantener el ID del archivo a eliminar
            deleteFileDialog: false , // Variable que activa el DIALOG de advertencia de eliminacion. 
            filesDrawer: false,
            dialogAddFile: false,
            editFileDialog: false,
            modelFileType: null,
            selectedFileType: '',
            newFile: '',
        }
    },

    methods: {
      
      /**
       * ------------------------------------
       *              MOSTRAR
       * ------------------------------------
      */

        /**
         * 
         * @param {Pago} payment 
         */
        loadFiles(model) {
          if (!model) {
              this.filesDrawer = false;
            } else {
              this.$root.$emit('openCommentsDrawer');
              this.filesDrawer = !this.filesDrawer;
              this.activeModel = model;
            }
        },

        /**
         * 
         * @param {Informacion del archivo} file 
         */
        getImagePath(file){
            if(file.mime_type != 'application/pdf') {
              if(file.thumbnail)
                  return this.assetsRoute + file.thumbnail.path;
                return this.assetsRoute + file.file_path;
            }
    
            return 'statics/images/pdf.png'
        },

        /**
         * ------------------------------------
         *              EDITAR
         * ------------------------------------
         */

         // ADJUNTAR UN ARCHIVO
        submitFile(){
          this.$store.commit("loading", true);
          const route = this.createRoute;
          const form = document.getElementById('submitFile');
          const data = new FormData(form);

          this.$axios.post(route, data).then( response => {
            this.$store.commit("loading", false);
            if (response) {
              /** Agregamos el archivo al pago seleccionado */
              this.activeModel.files.unshift(response.data);
              /** Desactivamos el dialog y limpiamos el formulario */
              this.dialogAddFile = false;
              this.clearFileForm();
              this.$q.notify({
                type: 'positive',
                message: 'Archivo adjuntado',
              });
            }
          }, error => {
            this.$store.commit("loading", false);
            return this.errorHandler(error.response.status, error.response.data);
          }).catch(
            error => {
              this.$store.commit("loading", false);
              return this.errorHandler(error.response.status, error.response.data);
            });
        },

        // LIMPIAR FORMULARIO
        clearFileForm(){
          this.selectedFileType = '';
          this.newFile = '';
        },

        /**
         * ------------------------------------
         *              EDITAR
         * ------------------------------------
         */
        showEditFileForm(file) {
          this.editFileDialog = true;
          this.fileID = file.id;
          this.modelFileType = file.file_type;
        },

        editFileSubmit(){
          this.$store.commit("loading", true);
          const uri = '/files/' + this.fileID;
          const route = this.$store.state.general.apiUrl + uri;
          const form = document.getElementById('editFileForm');
          const data = new FormData(form);
    
          this.$axios.post(route, data).then( response => {
            if( response ) {
              this.activeModel.files = this.activeModel.files.filter( file => {
                if(file.id === response.data.id) {
                  file.file_type = response.data.file_type;
                }

                return file;
              });

              this.$q.notify({
                type: 'positive',
                message: 'Archivo actializado correctamente'
              });
              this.$store.commit("loading", false);
              this.editFileDialog = false;
            }
          }, error => {
    
          });
        },


        /**
         * ------------------------------------
         *              ELIMINAR
         * ------------------------------------
         */

        /**
         * 
         * @param {ID del archivo a eliminar} fileID 
         */
        activeDeleteFileDialog(fileID) {
            this.deleteFileDialog = true;
            this.fileID = fileID;
        },
        
        /**
         * METODO PARA ELIMINAR UN ARCHIVO
         */
        deleteFile() {
            this.$store.commit("loading", true);
            this.deleteFileDialog = false;
            const route = this.$store.state.general.apiUrl + '/files/' + this.fileID;
      
            this.$axios.delete(route).then( response => {
              if(response) {
      
                const files = this.activeModel.files;
                this.activeModel.files = files.filter( file => {
                  return file.id != this.fileID
                });
      
                this.$store.commit("loading", false);
                this.$q.notify({
                  type: 'positive',
                  message: 'Archivo eliminado correctamente'
                });
              }
            }, error => {
              this.$store.commit("loading", false);
              return this.errorHandler(error.response.status, error.response.data);
            }). catch( error => {
              this.$store.commit("loading", false);
              this.$q.notify({
                  type: 'negative',
                  message: '¡UPS! algo salio mal'
              });
            })
        },
    }
}
