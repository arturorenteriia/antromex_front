export default {
    methods: {
        openCommentsDrawer: function(model) {
            this.$root.$emit('openCommentsDrawer', model)
            this.scrollTop();
        },
        
        hasComments: function(model) {
            let comments = 0;
            if(model.description) {
              comments = 1;
            }
      
            if(model.comments) {
              comments = parseInt(comments) + parseInt(model.comments.length);
            }
      
            return comments;
        },
    }
}