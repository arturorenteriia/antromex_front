export default {
    data() {
        return {
            requiredRule: [v => !!v || "Este campo es necesario"],
            passwordRule: [
                v => !!v || "El password es necesario",
                v => v.length > 7 || "Debe tener almenos 8 caracteres"
            ],
            passwordConfirmationRule: [
                v => !!v || "Este campo es necesario",
                v => v.length > 7 || "Debe tener almenos 8 caracteres",
                () => this.pwrd === this.pwrdc || "Las contraseñas no coinciden"
            ],
            emailRules: [
                v => !!v || "El e-mail es necesario",
                v => /.+@.+/.test(v) || "E-mail must be valid"
            ],
            emailConfirmationRules: [
                v => !!v || "Este campo es necesario",
                v => /.+@.+/.test(v) || "E-mail must be valid",
                () => this.email === this.emailConfirmation || "Los e-mail no coinciden"
            ]
        }
    },
    filters: {
        toPad(value) {
            return pad(value, 10);
        },
        toNumero(value) {
            let val = (value / 1).toFixed(0).replace(",", ",");
            return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }
    }
}