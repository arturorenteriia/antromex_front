export default {
    methods: {
        openFilesDrawer: function(model) {
            this.$root.$emit('openFilesDrawer', model)
            this.scrollTop();
        },
    }
}