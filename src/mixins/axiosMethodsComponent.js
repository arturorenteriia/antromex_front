export default {
    data() {
        return {}
    },

    methods: {
        /**
         * METODO PARA MANEJAR ERRORES
         */
        errorHandler: function (code, dataError){
            switch (code) {
                case 422:
                    this.listErrorMesages(dataError)
                    break;
                default:
                    this.$q.notify({
                        type: 'negative',
                        message: dataError.message,
                        position: 'bottom-right'
                    })
            }
        },

        /**
         * Metodo para crear una lista desordenada
         * @param {* Lista de errores} dataError 
         */
        listErrorMesages: function(dataError){
            const errors = dataError.errors;
            var htmlError = "<ul>";
            for (let error in errors) {
                console.log(errors[error]);
                htmlError += "<li>" + errors[error] + "</li>";
            }
            htmlError += "</ul>";

            this.$q.notify({
                type: 'negative',
                message: htmlError,
                position: 'bottom-right',
                html: true
                })

            return 
        },
    }
}